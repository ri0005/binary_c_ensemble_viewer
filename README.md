# binary_c_ensemble_viewer

Experimental viewer and plotter for binary_c's ensemble JSON data 

Original Qt5 JSON viewer based on Ashwin Nanjappa's code at
https://github.com/ashwin/json-viewer
(Apache licenced in LICENCE-json-viewer) - many thanks Ashwin!

Usage:

``ensemble_viewer <filename>``

where the file at <filename> contains the output from binary_c's ensemble.


----

note:

you should make sure you have python3.8 and latest module versions installed,
e.g. with

``pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U``

there are bugs in older versions of, e.g., traitlets, that will break things otherwise.


You will need to install (with pip3.8):

- PyQt5
- python-benedict
- seaborn
- traitlets
